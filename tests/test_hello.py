"""Module test function return print hello"""
from hello import hello

def test_hello():
    """Check the function hello"""
    assert hello() == "Hi"
