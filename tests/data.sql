INSERT INTO user(username, pwd)
VALUES
    ('test', 'pbkdf2:sha256:600000$DJc8tKQhXbrhrL8H$fb17ebc11fbd00d14da489d465615a495ef15f105c4966d02659556674d93e5f' ),
    ('test1', 'pbkdf2:sha256:600000$tX477vLAtjcRveLs$e76bb9e74cf8e16f35631b9041766d028d5f19d56ab8304ae36fd5079d15346b');

INSERT INTO employee_file(author_id,nam,surname,job,created_date,skill,experience,formation,seniority,body)
VALUES 
    ('1','WAE','Ben','ingénieur','20/06/2023','Analyse du besoin','NAVAL GROUP(2021-2023) Responsable recherche et innovation','Master MSI','débutant','En formation autodidacte');