"""Module test functions close open init db"""
import sqlite3
import pytest
from flaskr.db import get_db

def test_get_close_db(app):
    """Function test close db"""
    with app.app_context():
        database = get_db()
        assert database is get_db()
    with pytest.raises(sqlite3.ProgrammingError) as error:
        database.execute('SELECT 1')
    assert 'closed' in str(error.value)

def test_init_db_command(runner, monkeypatch):
    """Function test command init db"""
    class Recorder():
        """Class defined variable of called instance"""
        called = False
        def fake_init_db_error(self):
            """Function test call error"""
            Recorder.called = False
        def fake_init_db(self):
            """Function test call file in flaskr"""
            Recorder.called = True
        monkeypatch.setattr('flaskr.db.init_db', fake_init_db)
        result = runner.invoke(args=['init-db'])
        assert 'Initialized' in result.output
    assert Recorder.called
