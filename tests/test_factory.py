"""Module test function factory"""
from flaskr import create_app

def test_config():
    """Function testing config create.app"""
    assert not create_app().testing
    assert create_app({'TESTING: True'}).testing

def test_hello(client):
    """Function testing URL route of /hello"""
    response = client.get('/hello')
    assert response.data == b'Hello, Wrld'
