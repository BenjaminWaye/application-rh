"""Module test configure application"""
import os
import tempfile
from pytest import Pytester

from flaskr import create_app
from flaskr.db import get_db, init_db

with open(os.path.join(os.path.dirname(__file__), 'data.sql'), 'rb') as file:
    _data_sql = file.read().decode('utf8')


@Pytester.fixture
def test_db_connect_app():
    """Function test the way of db fictive/ call mode test"""
    db_fd, db_path = tempfile.mkstemp()
    app = create_app({
        'TESTING': True,
        'DATABASE': db_path,
    })
    with app.app_context():
        init_db()
        get_db().executescript(_data_sql)
    yield app
    os.close(db_fd)
    os.unlink(db_path)

@Pytester.fixture
def client(app):
    """Function test app without server"""
    return app.test_client()

@Pytester.fixture(name="test_runner")
def test_runner(app):
    "Function test app of click without server"
    return app.test_cli_runner()


class AuthActions():
    """Class test init, login and logout"""
    def __init__(self, customer):
        """Function init variable customer"""
        self._customer = customer

    def login(self,username="test", pwd='picasso'):
        """Function test loin with dabase fictive"""
        return self._customer.post(
            '/auth/login',
            data={'username': username, 'pwd': pwd}
        )
    def logout(self):
        """Function test redirection URL and error message"""
        return self._customer.get('/auth/logout')
@Pytester.fixture
def auth(customer):
    """Function test return value of customer"""
    return AuthActions(customer)
