import functools

from flask import(
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash

from flaskr.db import get_db

bp = Blueprint('auth', __name__, url_prefix='/auth')


# View authentification
@bp.route('/register', methods=('GET', 'POST'))
def register():
    if request.method == 'POST':
        username = request.form['username']
        pwd = request.form['pwd']
        db = get_db()
        error = None

        if not username:
            error = 'Identifiant'
        elif not pwd:
            error = 'Mot de passe requis.'
    
        if error is None:
            try: 
                db.execute(
                    "INSERT INTO user (username, pwd) VALUES (?, ?)",
                    (username, generate_password_hash(pwd)),
                )
                db.commit()
                error = f"L'identifiant {username} est inscrit."
            except db.IntegrityError:
                error = f"L'identifiant {username} est déjà utilisé."
        else:
            return redirect(url_for('auth.login'))
        
        flash(error)

    return render_template('auth/register.html')

# View connection
@bp.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        username = request.form['username']
        pwd = request.form['pwd']
        db = get_db()
        error = None
        user = db.execute(
            'SELECT * FROM user WHERE username = ?', (username,)
        ).fetchone()

        if user is None:
            error = f"Identifiant incorrect."
        elif not check_password_hash(user['pwd'], pwd):
            error = 'Mot de passe incorrect'

        if error is None:
            session.clear()
            session['user_id'] = user['id']
            return redirect(url_for("index"))
        
    return render_template('auth/login.html')

#Verify if the ID is in the db.
@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')

    if user_id is None:
        g.user = None
    else:
        g.user = get_db().execute(
            'SELECT * FROM user WHERE id = ?', (user_id,)
        ).fetchone()

# View deconnexion
@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))

#Check the auth in others views
def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))
        
        return view(**kwargs)
    return wrapped_view