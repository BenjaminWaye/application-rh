DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS employee_file;

CREATE TABLE user (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    username TEXT UNIQUE NOT NULL,
    pwd TEXT NOT NULL
);

CREATE TABLE employee_file (
    id_employee INTEGER PRIMARY KEY AUTOINCREMENT,
    author_id INTEGER NOT NULL,
    nam TEXT NOT NULL,
    surname TEXT NOT NULL,
    job TEXT NOT NULL,
    created_date TEXT,
    skill TEXT,
    experience TEXT,
    formation TEXT,
    seniority TEXT,
    body TEXT,
    FOREIGN KEY (author_id) REFERENCES user (id)
);

