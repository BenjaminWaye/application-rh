from flask import(
    Blueprint, flash, g, redirect, render_template, request,url_for
)

from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db
bp = Blueprint('gestion_rh', __name__)

@bp.route('/')
def index():
    db = get_db()
    profil_posts = db.execute(
        'SELECT e.id_employee, author_id, nam, surname, job, created_date, skill, experience, formation, seniority, body'
        ' FROM employee_file e JOIN user u ON e.author_id = u.id'
        ' ORDER BY created_date DESC'
    ).fetchall()
    return render_template('gestion_rh/index.html', profil_posts=profil_posts)

@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    if request.method == 'POST':
        nam = request.form['nam']
        surname = request.form['surname']
        job = request.form['job']
        created_date = request.form['created_date']
        skill = request.form['skill']
        experience = request.form['experience']
        formation = request.form['formation']
        seniority = request.form['seniority']
        body = request.form['body']
        error = None
    
        if not nam:
            error = 'Le nom du poste occupé est requit.'
        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO employee_file (nam, surname, job, created_date, skill, experience, formation, seniority, body, author_id)'
                ' VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
                (nam, surname, job, created_date, skill, experience, formation, seniority, body, g.user['id'])
            )
            db.commit()
            return redirect(url_for('gestion_rh.index'))
    
    return render_template('gestion_rh/create.html')

def get_employee_file(id_employee):
    employee_file = get_db().execute(
        'SELECT e.id_employee, author_id, nam, surname, job, created_date, skill, experience, formation, seniority, body'
        ' FROM employee_file e JOIN user u ON e.author_id = u.id'
        ' WHERE e.id_employee = ?',
        (id_employee,)
    ).fetchone()

    if employee_file is None:
        abort(404, f"La fiche de poste {id_employee} n'existe pas") 

    return employee_file

@bp.route('/<int:id_employee>/update', methods=('GET', 'POST'))
@login_required
def update(id_employee):
    employee_file = get_employee_file(id_employee)
    if request.method == 'POST':
        nam = request.form['nam']
        surname = request.form['surname']
        job = request.form['job']
        created_date = request.form['created_date']
        skill = request.form['skill']
        experience = request.form['experience']
        formation = request.form['formation']
        seniority = request.form['seniority']
        body = request.form['body']
        error = None
    
        if not nam :
            error = 'Le nom du poste occupé est requis.'
        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE employee_file SET nam = ?, surname = ?, job = ?, created_date = ?, skill = ?, experience = ?, formation = ?, seniority = ?, body = ?'
                ' WHERE id_employee = ?',
                (nam, surname, job, created_date, skill, experience, formation, seniority, body, id_employee)
            )
            db.commit()
            return redirect(url_for('gestion_rh.index'))
    
    return render_template('gestion_rh/update.html', employee_file=employee_file)

@bp.route('/<int:id_employee>/delete', methods=('POST',))
@login_required
def delete(id_employee):
    get_employee_file(id_employee)
    db = get_db()
    db.execute('DELETE FROM employee_file WHERE id_employee = ?', (id_employee,))
    db.commit()
    return redirect(url_for('gestion_rh.index'))
